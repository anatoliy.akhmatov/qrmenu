
<?php include_once 'functions.php'; ?>

<?php get_part('_parts/header') ?>

<div class="sections _has-mob">

	<div class="section _1">
		<?php get_part('_parts/small/arrow-down') ?>
		<div class="container">

			<div class="row align-items-center flex-column-reverse flex-lg-row">
				<div class="col-lg-5">
					<h1 class="title">Сontactless electronic qr-menu for сafe</h1>

					<ul class="section-features">
						<li><div class="section-features-icon"><?php get_part('img/features-thumb.svg') ?></div>updated in real time</li>
						<li><div class="section-features-icon"><?php get_part('img/features-thumb.svg') ?></div>works without app on any device</li>
						<li><div class="section-features-icon"><?php get_part('img/features-thumb.svg') ?></div>tips on the menu</li>
					</ul>

					<div class="section-actions">
						<button class="btn _big _dark">Try now</button>
						<button class="btn _big _outline-accent">
							<span class="btn-content">
								<span class="btn-text">sample menu</span>
							</span>
						</button>
					</div>
				</div>

				<div class="col-lg-7">
					<div class="section-img">
						<img src="img/intro.svg" alt="">
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="section _vertical _2">
		<?php get_part('_parts/small/arrow-down') ?>

		<div class="container">
			<div class="section-img">
				<img src="img/about.svg" alt="">
			</div>

			<h2 class="title-2">Wherever there is a menu</h2>

			<div class="description">
				For cafe & restaurants, lounge bars, hotels, night clubs, hookah bars and many others.
			</div>

			<button class="btn _outline-accent _big">
				<span class="btn-content">
					<span class="btn-text">why you need</span>
				</span>
			</button>
		</div>
	</div>

	<div class="section _3">
		<?php get_part('_parts/small/arrow-down') ?>

		<div class="container">
			<div class="row align-items-center flex-column-reverse flex-lg-row">
				<div class="col-lg-6">
					<h2 class="title-2">Better than printed menu</h2>

					<div class="description">
						No need to retype the menu to change the price or hide positions that are on the stop list - just open restik and edit the information.
					</div>

					<ul class="section-features _check">
						<li><div class="section-features-icon"><?php get_part('img/icons/check.svg') ?></div>does not break or deteriorate</li>
						<li><div class="section-features-icon"><?php get_part('img/icons/check.svg') ?></div>changing information and stop lists in two clicks in real time</li>
						<li><div class="section-features-icon"><?php get_part('img/icons/check.svg') ?></div>photos and description for each item</li>
					</ul>

					<button class="btn _big _outline-accent">
						<span class="btn-content">
							<span class="btn-text">and also</span>
						</span>
					</button>
				</div>

				<div class="col-lg-6">
					<div class="section-img">
						<img src="img/better.svg" alt="">
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="section _4">
		<?php get_part('_parts/small/arrow-down') ?>

		<div class="container">
			<div class="row align-items-center flex-column-reverse flex-lg-row">
				<div class="col-lg-6">
					<h2 class="title-2">Works anytime and anywhere</h2>

					<div class="description">
						Guests will be able to view the menu directly from their smartphone — just scan the qr-code placed on the table.
					</div>

					<ul class="section-features _check">
						<li><div class="section-features-icon"><?php get_part('img/icons/check.svg') ?></div>no need to wait for the waiter</li>
						<li><div class="section-features-icon"><?php get_part('img/icons/check.svg') ?></div>no need to install app</li>
						<li><div class="section-features-icon"><?php get_part('img/icons/check.svg') ?></div>works without app on any device</li>
					</ul>

					<button class="btn _big _outline-accent">
						<span class="btn-content">
							<span class="btn-text">business grow</span>
						</span>
					</button>
				</div>

				<div class="col-lg-6">
					<div class="section-img">
						<img src="img/anywhere.svg" alt="">
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="section _5">
		<?php get_part('_parts/small/arrow-down') ?>

		<div class="container">
			<div class="row align-items-center">

				<div class="col-lg-6">
					<div class="section-img d-none d-lg-block">
						<img src="img/help.svg" alt="">
					</div>
				</div>

				<div class="col-lg-6">
					<h2 class="title-2">Help businesses grow. It’s simple</h2>

					<ul class="section-features _number">
						<li><div class="section-features-icon"><?php get_part('img/steps-number-1.svg') ?></div>Register</li>
						<li><div class="section-features-icon"><?php get_part('img/steps-number-2.svg') ?></div>Fill in the menu</li>
						<li><div class="section-features-icon"><?php get_part('img/steps-number-3.svg') ?></div>Customize</li>
					</ul>

					<div class="section-img d-flex d-lg-none">
						<img src="img/help.svg" alt="">
					</div>

					<div class="description">Then print the qr-code and place it on the tables.</div>

					<div class="section-actions">
						<button class="btn _big _outline-accent">
							<span class="btn-content">
								<span class="btn-text">scan & test</span>
							</span>
						</button>
						<button class="btn _big _outline-dotted">
							<span class="btn-text">sample photo</span>
						</button>
					</div>
				</div>

			</div>
		</div>
	</div>

	<div class="section _6">
		<?php get_part('_parts/small/arrow-down') ?>

		<div class="container">
			<div class="row align-items-center">

				<div class="col-lg-6">
					<div class="section-img">
						<img src="img/demo.svg" alt="">
					</div>
				</div>

				<div class="col-lg-6">
					<h2 class="title-2">Just scan qr-code and test menu</h2>

					<div class="description">
						Guests will be able to view the menu directly from their smartphone — just scan the qr-code placed on the table.
					</div>

					<div class="section-actions _nohide">
						<button class="btn _big _outline-accent">
							<span class="btn-content">
								<span class="btn-text">advantages</span>
							</span>
						</button>
						<button class="btn _big _outline-dotted">
							<span class="btn-text">demo menu</span>
						</button>
					</div>
				</div>

			</div>
		</div>
	</div>

	<div class="section _vertical _7">
		<div class="section">

			<div class="features">

				<div class="features-item">
					<div class="features-item-img"><img src="img/features.svg" alt=""></div>
					<div class="features-item-content">
						<div class="features-item-number">~ 23%</div>
						<div class="features-item-description">
							increases the average check-guests order more
						</div>
					</div>
				</div>

				<div class="features-item">
					<div class="features-item-img"><img src="img/features-2.svg" alt=""></div>
					<div class="features-item-content">
						<div class="features-item-number">~ 31%</div>
						<div class="features-item-description">
							savings on staff and updating the printed menu
						</div>
					</div>
				</div>

			</div>

			<h2 class="title-2">Only perfect advantages</h2>

			<button class="btn _big _outline-accent">
				<span class="btn-content">
					<span class="btn-text">Launch qr menu</span>
				</span>
			</button>

		</div>
	</div>

</div>

<?php get_part('_parts/footer') ?>
