
<?php include_once 'functions.php'; ?>

<?php get_part('_parts/header') ?>

<!-- <div class="sections"> -->
	<div class="section _11 fp-completely">
		<div class="container">
			<div class="prices">
				<div>
					<h1 class="title _normal d-none d-lg-block">Choose your plan</h1>
					<div class="prices-items">

						<div class="swiper">
							<div class="swiper-wrapper">
								<div class="swiper-slide">

									<div class="prices-item">
										<div class="prices-item-header">
											<div class="prices-item-name">profy</div>
											<div class="prices-item-price">
												$0&nbsp;<span class='prices-item-duration'>/ month</span>
											</div>
										</div>

										<div class="prices-item-img">
											<img src="img/prices.svg" alt="">
										</div>


										<ul class="prices-item-features">
											<li>Free QR code</li>
											<li>Unlimited menu items & photos</li>
											<li>Menu Management</li>
											<li>Analytics</li>
											<li>WhatsApp and SMS Marketing tools</li>
											<li>Instagram account integration</li>
										</ul>

										<div class="prices-btn-wrap">
											<button class="btn _big _dark">get started</button>
										</div>
									</div>
								</div>

								<div class="swiper-slide">
									<div class="prices-item">
										<div class="prices-item-header">
											<div class="prices-item-name">advanced</div>
											<div class="prices-item-price">
												$15&nbsp;<span class='prices-item-duration'>/ month</span>
											</div>
										</div>

										<div class="prices-item-img">
											<img src="img/prices-2.svg" alt="">
										</div>


										<div class="prices-item-title">All from PROFY and:</div>
										<ul class="prices-item-features">
											<li>WhatsApp order notifications</li>
											<li>Online Ordering</li>
											<li>Payment Gateway Integration</li>
										</ul>

										<div class="spacer"></div>

										<div class="prices-btn-wrap">
											<button class="btn _big _dark">get started</button>
										</div>
									</div>
								</div>

								<div class="swiper-slide">
									<div class="prices-item">
										<div class="prices-item-header">
											<div class="prices-item-name">premium</div>
											<div class="prices-item-price">
												$30&nbsp;<span class='prices-item-duration'>/ month</span>
											</div>
										</div>

										<div class="prices-item-img">
											<img src="img/prices-3.svg" alt="">
										</div>


										<div class="prices-item-title">All from ADVANCED and:</div>
										<ul class="prices-item-features">
											<li>Payment Gateway Integration</li>
											<li>Custom Domain</li>
											<li>POS Integrations</li>
											<li>Complete Branding Control</li>
										</ul>

										<div class="spacer"></div>

										<div class="prices-btn-wrap">
											<button class="btn _big _dark">get started</button>
										</div>
									</div>
								</div>

							</div>

							<div class="swiper-nav">
								<div class="swiper-button-prev"><?php get_part('img/icons/next.svg') ?></div>
								<div class="swiper-pagination"></div>
								<div class="swiper-button-next"><?php get_part('img/icons/next.svg') ?></div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
	<!-- </div> -->

	<?php get_part('_parts/footer') ?>
