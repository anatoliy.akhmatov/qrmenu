<?php

global $hash;
$hash2 = filemtime('css/styles.css');
$hash3 = filemtime('js/base.js');
$hash = hash('ripemd160', $hash2 . $hash3);

function get_part($path, $vars = []) {
	global $hash;

	$vars = (object) $vars;
	$path = $path;
	if (strpos($path, '.') === false) {
		$path .= '.php';
	}
	$path = __DIR__ . '/' . $path;

	// if (file_exists($path))
	include $path;
}


