<!DOCTYPE html>
<html lang="ru" data-url="<?= $_SERVER['REQUEST_URI'] ?>">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, minimal-ui, initial-scale=1, maximum-scale=1, user-scalable=no" id="metaViewport">
	<meta name="apple-touch-fullscreen" content="yes" />
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />

	<title>QR MENU</title>
	<meta name="description" content="">
	<meta property="og:description" content="">

	<link rel="apple-touch-icon" sizes="180x180" href="/img/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/img/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/img/favicon/favicon-16x16.png">
	<link rel="manifest" href="/img/favicon/site.webmanifest">
	<link rel="mask-icon" href="/img/favicon/safari-pinned-tab.svg" color="#5bbad5">
	<link rel="shortcut icon" href="/img/favicon/favicon.ico">
	<meta name="msapplication-TileColor" content="#ff0000">
	<meta name="msapplication-config" content="/img/favicon/browserconfig.xml">

	<meta name="theme-color" content='#34574F'>
	<meta name="msapplication-navbutton-color" content='#34574F'>
	<meta name="apple-mobile-web-app-status-bar-style" content='#34574F'>

	<link rel="stylesheet" href="css/styles.css">
</head>

<body>
	<div id="app">
		<div id="loader" class='active'></div>

		<div class="lines">
			<div class="lines-item"></div>
			<div class="container">
				<div class="lines-item"></div>
				<div class="lines-item"></div>
				<div class="lines-item"></div>
				<div class="lines-item"></div>
				<div class="lines-item"></div>
			</div>
			<div class="lines-item"></div>
		</div>

		<header class="header">
			<div class="container">
				<div class="header-content">
					<div class="menu-btn">
						<button class="hamburger hamburger--elastic" type="button">
							<span class="hamburger-box">
								<span class="hamburger-inner"></span>
							</span>
						</button>
					</div>

					<a href='#' class="header-logo">
						<img src="img/logo.svg" alt="">
					</a>
					<ul class="header-menu">
						<li><a href="#" class='active'>QR menu</a></li>
						<li><a href="#">Delviery</a></li>
						<li><a href="#">Prices</a></li>
						<li><a href="#">Blog</a></li>
						<li><a href="#">Faq</a></li>
						<li><a href="#">Contacts Us</a></li>
					</ul>

					<div class="spacer d-none d-lg-block"></div>

					<div class="langs select">
						<select>
							<option value="eng" selectes>Eng</option>
							<option value="ru">Ru</option>
						</select>
					</div>

					<div class="header-actions">
						<a href='#' class="btn _outline _login">
							<div class="btn-text">Login</div>
							<div class="btn-icon"><?php get_part('img/icons/arrow.svg') ?></div>
						</a>

						<div class="btn _accent">
							<div class="btn-text">Try Free</div>
						</div>
					</div>

					<a href="#" class="header-mob-account">
						<?php get_part('img/icons/login.svg') ?>
					</a>
				</div>
			</div>
		</header>

		<div class="mob">

		</div>

		<div id="content">

			<div id="menu"></div>
