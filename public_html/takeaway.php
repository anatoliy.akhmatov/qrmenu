
<?php include_once 'functions.php'; ?>

<?php get_part('_parts/header') ?>

<div class="sections">

	<div class="section _8">
		<?php get_part('_parts/small/arrow-down') ?>

		<div class="container">
			<div class="row align-items-center">

				<div class="col-xl-6 col-lg-5">
					<div class="section-img">
						<img src="img/landing.png" class='d-none d-sm-block' alt="">
						<img src="img/landing-mob.png" class='d-block d-sm-none' alt="">
					</div>
				</div>

				<div class="col-xl-6 col-lg-7">
					<h1 class="title">
						Digital menu online TakeAway & Pick Up
					</h1>

					<ul class="section-features _even">
						<li><div class="section-features-icon"><?php get_part('img/icons/check.svg') ?></div>We will print QR code stickers for your business.</li>
					</ul>

					<div class="description _light d-none d-lg-block">
						Grow your business using the power of cloud-based technology integrated with online/offline POS systems. Multiple ways to satisfy customers by offering simplified QR code tableside ordering, delivery, takeaway, contactless payments and many more.
					</div>

					<div class="section-actions">
						<button class="btn _big _dark">pricing</button>
						<button class="btn _big _outline-dotted">
							<span class="btn-content">
								<span class="btn-text">sample menu</span>
							</span>
						</button>
					</div>
				</div>

			</div>

			<div class='d-block d-lg-none'>

				<?php get_part('_parts/small/arrow-down') ?>

				<div class="description _light">
					Grow your business using the power of cloud-based technology integrated with online/offline POS systems. Multiple ways to satisfy customers by offering simplified QR code tableside ordering, delivery, takeaway, contactless payments and many more.
				</div>

				<button class="btn _big _outline-dotted">
					<span class="btn-content">
						<span class="btn-text">sample menu</span>
					</span>
				</button>
			</div>
		</div>
	</div>

	<div class="section _features-slider _9">
		<?php get_part('_parts/small/arrow-down') ?>

		<div class="container">
			<div class="row align-items-center">

				<div class="col-lg-6">
					<h1 class="title">
						Our key features
					</h1>

					<div class="description _light">
						Say goodbye to third-party fees with Menu4you's direct online ordering at zero commission!
					</div>

					<div class='d-none d-lg-block'>
						<div class="section-features-menu">
							<label class="checkbox _1 _active">
								<input type="radio" name='feature' checked value='1'>
								<span class="checkbox-content">
									<span class="checkbox-thumb"></span>
									<span class="checkbox-text">
										Multilingual Digital Menu
									</span>
								</span>
							</label>

							<label class="checkbox _2">
								<input type="radio" name='feature' value='2'>
								<span class="checkbox-content">
									<span class="checkbox-thumb"></span>
									<span class="checkbox-text">
										Scan & Order
									</span>
								</span>
							</label>

							<label class="checkbox _3">
								<input type="radio" name='feature' value='3'>
								<span class="checkbox-content">
									<span class="checkbox-thumb"></span>
									<span class="checkbox-text">
										Multi-language Mobile & Waiter Ordering
									</span>
								</span>
							</label>

							<label class="checkbox _4">
								<input type="radio" name='feature' value='4'>
								<span class="checkbox-content">
									<span class="checkbox-thumb"></span>
									<span class="checkbox-text">
										Addons, Variants & Ingredients
									</span>
								</span>
							</label>
						</div>

						<button class="btn _big _outline-accent">
							<span class="btn-content">
								<span class="btn-text">How it works</span>
							</span>
						</button>
					</div>
				</div>

				<div class="col-lg-6">
					<div class="section-features-wrap">
						<div class="section-features-slider">
							<div class="swiper">
								<div class="swiper-wrapper">
									<div class="swiper-slide">
										<img src="img/landing-features-2.png" alt="">
									</div>

									<div class="swiper-slide">
										<img src="img/landing-features-1.png" alt="">
									</div>

									<div class="swiper-slide">
										<img src="img/landing-features-3.png" alt="">
									</div>

									<div class="swiper-slide">
										<img src="img/landing-features-4.png" alt="">
									</div>
								</div>
							</div>

							<div class="swiper-button-prev"><?php get_part('img/icons/next.svg') ?></div>
							<div class="swiper-button-next"><?php get_part('img/icons/next.svg') ?></div>
						</div>
					</div>

					<div class="d-flex d-lg-none flex-column align-items-center">
						<div class="section-features-menu">
							<label class="checkbox _1 _active">
								<input type="radio" name='feature' checked value='1'>
								<span class="checkbox-content">
									<span class="checkbox-thumb"></span>
									<span class="checkbox-text">
										Multilingual Digital Menu
									</span>
								</span>
							</label>

							<label class="checkbox _2">
								<input type="radio" name='feature' value='2'>
								<span class="checkbox-content">
									<span class="checkbox-thumb"></span>
									<span class="checkbox-text">
										Scan & Order
									</span>
								</span>
							</label>

							<label class="checkbox _3">
								<input type="radio" name='feature' value='3'>
								<span class="checkbox-content">
									<span class="checkbox-thumb"></span>
									<span class="checkbox-text">
										Multi-language Mobile & Waiter Ordering
									</span>
								</span>
							</label>

							<label class="checkbox _4">
								<input type="radio" name='feature' value='4'>
								<span class="checkbox-content">
									<span class="checkbox-thumb"></span>
									<span class="checkbox-text">
										Addons, Variants & Ingredients
									</span>
								</span>
							</label>
						</div>

						<button class="btn _big _outline-accent">
							<span class="btn-content">
								<span class="btn-text">How it works</span>
							</span>
						</button>
					</div>

				</div>
			</div>

		</div>
	</div>

	<div class="section _10">
		<div class="container">
			<div class="steps">
				<h2 class="title">How it works</h2>

				<div class="steps-items">
					<div class="steps-item">
						<div class="steps-img"><img src="img/steps-1.svg" alt=""></div>
						<div class="steps-bottom">
							<div class="steps-number">
								<img src="img/steps-number-1.svg" alt="">
							</div>
							<div class="steps-description">
								You scan QR code and order
							</div>
						</div>
					</div>

					<div class="steps-item">
						<div class="steps-img"><img src="img/steps-2.svg" alt=""></div>
						<div class="steps-bottom">
							<div class="steps-number">
								<img src="img/steps-number-2.svg" alt="">
							</div>
							<div class="steps-description">
								The chef in the cafe got and cooks
							</div>
						</div>
					</div>

					<div class="steps-item">
						<div class="steps-img"><img src="img/steps-3.svg" alt=""></div>
						<div class="steps-bottom">
							<div class="steps-number">
								<img src="img/steps-number-3.svg" alt="">
							</div>
							<div class="steps-description">
								Food’ll be deliveried or TakeAway
							</div>
						</div>
					</div>

					<div class="steps-item">
						<div class="steps-img"><img src="img/steps-4.svg" alt=""></div>
						<div class="steps-bottom">
							<div class="steps-number">
								<img src="img/steps-number-4.svg" alt="">
							</div>
							<div class="steps-description">
								Food’ll be deliveried or TakeAway
							</div>
						</div>
					</div>
				</div>

				<div class="steps-button-wrap">
					<button class="btn _big _outline-accent">
						<span class="btn-content">
							<span class="btn-text">any questions</span>
						</span>
					</button>
				</div>

			</div>
		</div>
	</div>
</div>

<?php get_part('_parts/footer') ?>
