
<?php include_once 'functions.php'; ?>

<?php get_part('_parts/header') ?>

<div class="sections _has-mob">

	<div class="section _12">
		<div class="contact">
			<div class="container">
				<div class="contact-wrap">
					<div class="contact-img"><img src="img/contact.svg" alt=""></div>

					<h1 class="title _normal">We are ready to help you</h1>

					<div class="description _big">
						Talk to our sales team, get support in regard to our products or services.
					</div>

					<div class="socials">
						<a href='#' class="socials-item _blue">
							<span class="socials-item-content">
							<div class="socials-icon"><img src="img/icons/telegram.svg" alt=""></div>
							<div class="socials-text">telegram</div>
							</span>
						</a>
						<a href='#' class="socials-item _green">
							<span class="socials-item-content">
							<div class="socials-icon"><img src="img/icons/wp.svg" alt=""></div>
							<div class="socials-text">whatsapp</div>
							</span>
						</a>
						<a href='#' class="socials-item">
							<span class="socials-item-content">
							<div class="socials-icon"><img src="img/icons/inst.svg" alt=""></div>
							<div class="socials-text">instagram</div>
							</span>
						</a>
					</div>

					<a href="mailto:menu4you@gmail.com" class="contact-email">menu4you@gmail.com</a>

					<div class="spacer"></div>
					<div class="contact-down">
						<div class="contact-down-text">Feedback</div>
						<?php get_part('_parts/small/arrow-down') ?>
					</div>

				</div>
			</div>
		</div>
	</div>

	<div class="section _13">
		<div class="contact-form">
			<div class="container">
					<div class="contact-form-content">
					<h2 class="title _normal">Get in Touch</h2>

					<div class="contact-form-inputs">

						<div class="input">
							<div class="input-icon"><?php get_part('img/icons/account.svg') ?></div>
							<input type="text" placeholder='name'>
						</div>

						<div class="input">
							<div class="input-icon"><?php get_part('img/icons/mail.svg') ?></div>
							<input type="text" placeholder='e-mail'>
						</div>

						<div class="input">
							<div class="input-icon"><?php get_part('img/icons/phone.svg') ?></div>
							<input type="text" placeholder='phone'>
						</div>

					</div>

					<div class="label">Your question or message</div>
					<div class="input">
						<textarea></textarea>
					</div>

					<button class="btn _big _dark">
						<span class="btn-text">send request</span>
					</button>
					</div>
			</div>
		</div>
	</div>

</div>


<?php get_part('_parts/footer') ?>