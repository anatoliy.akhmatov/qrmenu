
<?php include_once 'functions.php'; ?>

<?php get_part('_parts/header') ?>

<div class="sections">

	<div class="section">

		<div class="container">

			<h1 class="title">
				404 Page not found
			</h1>

			<div class="description _light">
				The page was not found, but you can go to the main page of <a href="#">our site</a>
			</div>

		</div>
	</div>

</div>

<?php get_part('_parts/footer') ?>