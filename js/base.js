
import './modules/helpers/share.js'

import extend from 'deepmerge'; share(extend, 'extend')
import '../libs/multiple-select-js/dist/js/multiple-select.js'

import '../libs/fullPage.js/dist/fullpage.js'

import Swiper from 'swiper'; share(Swiper)
// Chart.register(ChartDataLabels);

import 'magnific-popup'

import './modules/main/core.js'

import App from './modules/helpers/app.js'; share(App, 'App')
import Check from './modules/helpers/check.js'
import Scroll from './modules/helpers/scroll.js'
import Lazy from './modules/helpers/lazy.js'

import Config from './modules/main/config.js'
import Main from './modules/main/main.js'

import Polotno from './modules/includes/polotno.js'
import Menu from './modules/includes/menu.js'
// import Submit from './modules/main/submit.js'

import Loader from './modules/includes/loader.js'
import Select from './modules/includes/select.js'
import Modal from './modules/includes/modal.js'

window.appConfig = {
	modules: [
		Config,
		Check,

		Scroll,
		// Submit,
		Polotno,
		Main,
		Menu,
		Lazy,
		Select,

		Modal,

		Loader
		],
}

new App(appConfig)













