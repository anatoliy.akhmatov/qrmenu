
export default class Lazy {
	constructor() {
		this.offset = 300

		window.addEventListener('resize', debounce(this.init.bind(this), 50))
		window.addEventListener('scroll', debounce(this.init.bind(this), 50))

		app.on('init', () => {
			this.init()
		})
	}

	init() {
		$('[data-src]:not(._inited):not(.swiper-lazy)').foreach(el => {
			if (el && el.offsetTop < window.innerHeight + window.pageYOffset + this.offset) {
				this.load(el)
			}
		})
	}

	load(el) {
		let src = el.getAttribute('data-src')
		if (el.tagName == 'IMG') {
			el.src = src
		} else {
			el.style.backgroundImage = `url('${src}')`
		}
		el.classList.add('_inited')
	}
}

