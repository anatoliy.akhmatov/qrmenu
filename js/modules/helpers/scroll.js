
export default class Scroll {
	constructor() {
		var disableScroll = false;
		var scrollPos = 0;

		this.disable = function() {
			disableScroll = true;
			// scrollPos = $(window).scrollTop();
		}

		this.enable = function() {
			disableScroll = false;
		}

		// $(function(){
		// 	$(window).bind('scroll', function(){
		// 		if(disableScroll) $(window).scrollTop(scrollPos);
		// 	});
		// 	$(window).bind('touchmove', function(){
		// 		$(window).trigger('scroll');
		// 	});
		// });
	}
}