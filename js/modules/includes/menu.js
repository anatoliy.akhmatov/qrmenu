

// https://gist.github.com/AnatoliyAkhmatov/a2862794ce5cdbc00ecb88fc3c8e4c86

export default class Menu {
	constructor(app) {
		this.app = app
		var self = this

		app.on('init', () => {
			this.mobMenu = $('.mob')
			this.mobMenuBtn = $('.menu-btn')
			// this.headerTop = document.querySelector('.header-top')

			this.mobMenuBtn.click(self.toggle.bind(this))

			this.mobMenu.click(e => {
				if (!e.target.closest('.mob-content')) {
					this.close()
				}
			})

			// document.addEventListener('scroll', self.scroll.bind(this))
		})
	}

	toggle() {
		this.mobMenu.hasClass('_active') ? this.close() : this.open()
	}

	close() {
		this.mobMenuBtn.find('.hamburger').removeClass('is-active')
		this.mobMenu.removeClass('_active')

		this.app.scroll.enable()
	}

	open() {
		this.mobMenuBtn.find('.hamburger').addClass('is-active')
		this.mobMenu.addClass('_active')

		this.app.scroll.disable()


		self.app.main.closeOther('menu')
	}

	// scroll() {
	// 	this.headerTop.classList[window.pageYOffset >= 40 ? 'add' : 'remove']('_min')
	// }
}