
export default class Select {
	constructor(app) {

		app.on('init', () => {
			this.init()
		})
	}

	init() {
		if (document.querySelector('.multiselect select:not(.inited)')) {

			$('.multiselect select:not(.inited)').each(function() {
				var $el = $(this)

				var id = randomKey()
				$el.attr('data-id', id)

				var placeholder = $el.attr('placeholder') || '⠀'

				new MultipleSelect(`[data-id='${id}']`, {
					placeholder
				})

				$el.addClass('inited')
			})
		}

		if (document.querySelector('.select select:not(.inited)')) {
			$('.select select:not(.inited)').each(function() {
				var $el = $(this)

				var id = randomKey()
				$el.attr('data-id', id)

				var placeholder = $el.attr('placeholder') || '⠀'

				new MultipleSelect(`[data-id='${id}']`, {
					placeholder
				})

				$el.addClass('inited')
			})
		}
	}
}
