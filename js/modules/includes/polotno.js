

export default class Polotno {
	constructor(app) {

		app.on('init', () => {
			this.featuresSlider()
			this.section()
			this.pricesSlider()
		})
	}

	pricesSlider() {
		let container = document.querySelector('.prices .prices-items')
		if (!container) return;
		let $container = $(container)

		let slider = container.querySelector('.swiper')

		let prevEl = slider.parentNode.querySelector('.swiper-button-prev')
		let nextEl = slider.parentNode.querySelector('.swiper-button-next')
		let pagination = slider.parentNode.querySelector('.swiper-pagination')

		let sliderConfig = {
			slidesPerView: window.innerWidth > 992 ? 3 : 1,
			 // freemode: true,
			loop: false,
			speed: 200,
			allowTouchMove: window.innerWidth > 992 ? false : true,

			observer: true,
			observeParents: true,
			observeSlideChildren: true,

			spaceBetween: window.innerWidth > 1200 ? 26 : 18,

			pagination: {
				el: pagination,
				type: 'bullets',
				clickable: true
			},

			lazy: {
				loadPrevNext: true,
			},
			preloadImages: false,

			init: true,

			navigation: {
				nextEl,
				prevEl,
			},
			autoplay: false
		}

		const sl = new Swiper(slider, sliderConfig);
	}

	featuresSlider() {
		let container = document.querySelector('.section._features-slider')
		if (!container) return;
		let $container = $(container)

		let slider = container.querySelector('.swiper')

		let prevEl = slider.parentNode.querySelector('.swiper-button-prev')
		let nextEl = slider.parentNode.querySelector('.swiper-button-next')

		let sliderConfig = {
			slidesPerView: 1,
			 // freemode: true,
			loop: true,
			speed: 200,
			allowTouchMove: true,

			observer: true,
			observeParents: true,
			observeSlideChildren: true,

			spaceBetween: 20,

			lazy: {
				loadPrevNext: true,
			},
			preloadImages: false,

			init: true,

			navigation: {
				nextEl,
				prevEl,
			},
			autoplay: false
		}

		const sl = new Swiper(slider, sliderConfig);

		$container.find('.section-features-menu').on('click', '.checkbox', function(e) {
			e.preventDefault()
			let $el = $(this)
			dd('click')

			$container.find('.checkbox._active').removeClass('_active').find('input').removeAttr('checked')
			let $input = $el.find('input')
			$el.addClass('_active')
			$input.attr('checked', 'checked')
			let n = $input.val()

			sl.slideTo(n - 1)
		})

		sl.on('slideChange', () => {
			let index = sl.realIndex + 1

			$container.find('.checkbox._active').removeClass('_active').find('input').removeAttr('checked')
			$container.find(`.checkbox._${index}`).addClass('_active').find('input').attr('checked', 'checked')
		})
	}

	section() {
		new Promise((resolve) => {
			setTimeout(function() {
				window.scrollTo(0, 1);
				resolve()
			}, 0)
		}).then(() => {

			$('.arrow-down').click(() => {
				$.fn.fullpage.moveSectionDown()
			})

			let $sections = $('.sections')
			let hasMob = $sections.hasClass('_has-mob')
			if ((hasMob || (window.innerHeight > 500 && window.innerWidth > 992)) && $sections[0]) {
				document.html.classList.add('_sections')

				$sections.fullpage({
					autoScrolling: true,

					anchors: [],

					dragAndMove: true,
					scrollOverflow: false,
				// bigSectionsDestination: null,
					paddingTop: 0,
					paddingBottom: 0,

					// css3: false,
					navigation: true,
					slidesNavigation: true,
					fitToSection: true,
					fitToSectionDelay: 1000,
					bigSectionsDestination: 'top',
					continuousVertical: true,
					// responsiveHeight: 330,
					controlArrows: false,

					scrollBar: false,
					verticalCentered: true,

					sectionSelector: '.section',
					credits: {enabled: false}
				});
			} else {
				$sections.find('.section').addClass('fp-completely')
				document.html.classList.add('_scrolling')
			}
		})
	}
}