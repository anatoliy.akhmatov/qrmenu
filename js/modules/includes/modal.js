
function onModalOpen(name, f) {
	$(document).on('js-modal-open', () => {
		let $modal = $('.mfp-content .t-modal[data-mfp]')
		let modalName = $modal.data('mfp')
		if (modalName != name) return;
		else f($modal[0])
	})
}

export default class Modal {
	constructor(app) {
		this.app = app

		var self = this
		this.config = {
			type:'inline',
			midClick: true,
			preloader: false,
			mainClass: 'mfp-fade',
			removalDelay: 300,
			autoFocusLast: false,
			tClose: 'Закрыть (Esc)',
			tLoading: 'Загрузка...',
			closeMarkup: `
			<button title="%title%" type="button" class="mfp-close">
			<svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
			<path d="M7 5.58623L11.95 0.63623L13.364 2.05023L8.414 7.00023L13.364 11.9502L11.95 13.3642L7 8.41423L2.05 13.3642L0.636 11.9502L5.586 7.00023L0.636 2.05023L2.05 0.63623L7 5.58623Z" fill="#242B40"/>
			</svg>
			</button>`,
			// closeOnBgClick: false,
			closeOnBgClick: window.innerWidth > 768,
			callbacks: {
				open() {
					$('.mfp-content .modal-close').click(() => $.magnificPopup.close())
					self.app.scroll.disable()
					$('body').trigger('js-modal-open')
				},
				close() {
					self.app.scroll.enable()
					$('body').trigger('js-modal-close')
				}
			}
		}

		this.configImg = {
			type:'image',
			mainClass: 'mfp-with-zoom',
			// closeOnBgClick: true,
			zoom: {
				enabled: true,
				duration: 300,
				easing: 'ease-in-out',
				opener: function(openerElement) {
					return openerElement.is('img') ? openerElement : openerElement.find('img');
				}
			}
		}

		this.configGallery = {
			gallery: {
				enabled: true,
				preload: [0, 2],
				tPrev: 'Назад',
				tNext: 'Вперед',
				tCounter: '<span class="mfp-counter">%curr% из %total%</span>'
			},
		}

		app.on('init', () => {
			this.initInline()

			// this.inlineOpen('offer')
		})
	}

	inlineOpen(name) {
		setTimeout(() => {
			let modal = $(`[data-mfp='${name}']`)
			$.magnificPopup.open($.extend(this.config, {
				type: 'inline',
				items: {
					src: modal
				}
			}))

			if (modal.hasClass('downup')) {
				setTimeout(() => {
					$('.mfp-wrap').addClass('downup')
				}, config.animation.transition + 10)
			}
		}, 10)
	}

	initInline() {
		let self = this

		$('[data-mfp-src]').click(function(e) {
			e.preventDefault()

			self.inlineOpen($(this).data('mfp-src'))
		})
	}

	close() {
		$.magnificPopup.close()
	}
}
