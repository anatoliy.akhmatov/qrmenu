
global.config = {
  mode: 'dev',
  path: {
    html: 'public_html',
    img: 'public_html/img',
    css: [
      'sass',
      'public_html/css'
    ],
    js: [
      'js',
      'public_html/js'
    ]
  }
}